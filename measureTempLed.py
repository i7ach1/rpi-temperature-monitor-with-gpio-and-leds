#! /usr/bin/env python
"""
Author:		Ari 'i7ach1' Mirabal.
Web:		https://mirabalphoto.es
Mail:		a.mirabal@mirabalphoto.es
About this program:
	This program is a little CPU temperature monitor for Raspberry Pi.
	It use 10 led diodes to show us the temperature range between 35C and 85C
	using ranges of 5C between leds.

Short guide about leds and GPIO used for it.
I used 2 leds white, 2 leds blue, 2 leds green, 2 leds yellow and 2 leds red.

	Spanish		GPIO used	Def name		English
	rojo 2		->	gpio2	-> r2 (on/off)	// red led 2
	rojo 1		->	gpio3	-> r1 (on/off)	// red led 1
	amarillo 2	->	gpio4	-> am2 (on/off)	// yellow led 2
	amarillo 1	->	gpio17	-> am1 (on/off)	// yellow led 1
	verde 2		->	gpio27	-> v2 (on/off)	// green led 2
	verde 1		->	gpio22	-> v1 (on/off)	// green led 1
	azul 2		->	gpio10	-> az2 (on/off)	// blue led 2
	azul 1		->	gpio9	-> az1 (on/off)	// blue led 1
	blanco 2	->	gpio11	-> b2 (on/off)	// white led 2
	blanco 1	->	gpio5	-> b1 (on/off)	// white led 1
"""
import RPi.GPIO as GPIO
import time
import re
import os
GPIO.setmode(GPIO.BCM)
def b1on():
	GPIO.setup(5, GPIO.OUT)
	GPIO.output(5, True)
def b1off():
	GPIO.setup(5, GPIO.OUT)
	GPIO.output(5, False)
def b2on():
	GPIO.setup(11, GPIO.OUT)
	GPIO.output(11, True)
def b2off():
	GPIO.setup(11, GPIO.OUT)
	GPIO.output(11, False)
def az1on():
	GPIO.setup(9, GPIO.OUT)
	GPIO.output(9, True)
def az1off():
	GPIO.setup(9, GPIO.OUT)
	GPIO.output(9, False)
def az2on():
	GPIO.setup(10, GPIO.OUT)
	GPIO.output(10, True)
def az2off():
	GPIO.setup(10, GPIO.OUT)
	GPIO.output(10, False)
def v1on():
	GPIO.setup(22, GPIO.OUT)
	GPIO.output(22, True)
def v1off():
	GPIO.setup(22, GPIO.OUT)
	GPIO.output(22, False)
def v2on():
	GPIO.setup(27, GPIO.OUT)
	GPIO.output(27, True)
def v2off():
	GPIO.setup(27, GPIO.OUT)
	GPIO.output(27, False)
def am1on():
	GPIO.setup(17, GPIO.OUT)
	GPIO.output(17,True)
def am1off():
	GPIO.setup(17, GPIO.OUT)
	GPIO.output(17,False)
def am2on():
	GPIO.setup(4, GPIO.OUT)
	GPIO.output(4, True)
def am2off():
	GPIO.setup(4, GPIO.OUT)
	GPIO.output(4, False)
def r1on():
	GPIO.setup(3, GPIO.OUT)
	GPIO.output(3, True)
def r1off():
	GPIO.setup(3, GPIO.OUT)
	GPIO.output(3, False)
def r2on():
	GPIO.setup(2, GPIO.OUT)
	GPIO.output(2, True)
def r2off():
	GPIO.setup(2, GPIO.OUT)
	GPIO.output(2, False)
def limpiar():
	GPIO.cleanup() 	# Clean_all GPIO
def measure_temp():		# Take CPU measure temp and return an INT value.
    raw = os.popen('vcgencmd measure_temp').readline()
    m = re.match("temp=(\d+\.?\d*)'C", raw)
    if not m:
        raise ValueError("Unexpected temperature string: " + raw)
    return int(float(m.group(1)))
try:
	while True:
		temp = measure_temp()
		if temp >= 35 and temp < 40:
			b1on()
			b2off()
			az1off()
			az2off()
			v1off()
			v2off()
			am1off()
			am2off()
			r1off()
			r2off()
		elif temp >= 40 and temp < 45:
			b1on()
			b2on()
			az1off()
			az2off()
			v1off()
			v2off()
			am1off()
			am2off()
			r1off()
			r2off()
		elif temp >= 45 and temp < 50:
			b1on()
			b2on()
			az1on()
			az2off()
			v1off()
			v2off()
			am1off()
			am2off()
			r1off()
			r2off()
		elif temp >= 50 and temp < 55:
			b1on()
			b2on()
			az1on()
			az2on()
			v1off()
			v2off()
			am1off()
			am2off()
			r1off()
			r2off()
		elif temp >= 55 and temp < 60:
			b1on()
			b2on()
			az1on()
			az2on()
			v1on()
			v2off()
			am1off()
			am2off()
			r1off()
			r2off()
		elif temp >= 60 and temp < 65:
			b1on()
			b2on()
			az1on()
			az2on()
			v1on()
			v2on()
			am1off()
			am2off()
			r1off()
			r2off()
		elif temp >= 65 and temp < 70:
			b1on()
			b2on()
			az1on()
			az2on()
			v1on()
			v2on()
			am1on()
			am2off()
			r1off()
			r2off()
		elif temp >= 70 and temp < 75:
			b1on()
			b2on()
			az1on()
			az2on()
			v1on()
			v2on()
			am1on()
			am2on()
			r1off()
			r2off()
		elif temp >= 75 and temp < 80:
			b1on()
			b2on()
			az1on()
			az2on()
			v1on()
			v2on()
			am1on()
			am2on()
			r1on()
			r2off()
		elif temp >= 80 and temp < 85:
			b1on()
			b2on()
			az1on()
			az2on()
			v1on()
			v2on()
			am1on()
			am2on()
			r1on()
			r2on()
		time.sleep(1)
except KeyboardInterrupt:
	limpiar()
